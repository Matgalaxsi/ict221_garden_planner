/**
 * This package implements a simple Garden Planner costing program.
 *
 * See GardenPlanner for the main class.
 *
 * @author Mark Utting
 *
 */
package garden_planner;